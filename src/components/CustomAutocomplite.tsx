import * as React from 'react';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm } from 'react-hook-form';

const filter = createFilterOptions<OptionType>();

export interface OptionType {
    inputValue?: string;
    name: string;
    id?: string;
}
interface CustomSelectProps {
    options: OptionType[];
    addNewOption: (option: OptionType) => void;
    label: string;
}
export const validationSchema = yup.object().shape({
    name: yup.string().required(),
    id: yup.string().required(),
});
const CustomSelect: React.FC<CustomSelectProps> = ({ options ,addNewOption, label }) => {
    const [value, setNewValue] = React.useState<OptionType | null>(null);
    const [open, toggleOpen] = React.useState(false);
    const {
        handleSubmit,
        setValue,
        getValues,
        formState: { errors },
    } = useForm<OptionType>({ resolver: yupResolver(validationSchema) });

    const handleClose = () => {
        setValue("name", "")
        setValue("id", "")
        toggleOpen(false);
    };

    const onSubmit:SubmitHandler<OptionType> = ({name, id}) => {
            setNewValue({
                name: name,
                id: id,
            });
            addNewOption({
                name: name,
                id: id,
            })
        handleClose();
    };

    return (
        <React.Fragment>
            <Autocomplete
                style={{marginBottom: "25px"}}
                value={value}
                onChange={(event, newValue) => {
                    if (typeof newValue === 'string') {
                        // timeout to avoid instant validation of the dialog's form.
                        setTimeout(() => {
                            toggleOpen(true);
                            setValue("name", newValue)
                        });
                    } else if (newValue && newValue.inputValue) {
                        toggleOpen(true);

                        setValue("name", newValue.inputValue)
                    } else {
                        setNewValue(newValue);
                    }
                }}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);

                    if (params.inputValue !== '') {
                        filtered.push({
                            inputValue: params.inputValue,
                            name: `Add "${params.inputValue}"`,
                        });
                    }

                    return filtered;
                }}
                id="free-solo-dialog-demo"
                options={options}
                filterSelectedOptions
                getOptionLabel={(option) => {
                    if (typeof option === 'string') {
                        return option;
                    }
                    if (option.inputValue) {
                        return option.inputValue;
                    }
                    return option.name;
                }}
                selectOnFocus
                clearOnBlur={false}
                handleHomeEndKeys
                renderOption={(props, option) => <li {...props} key={option.id}>{option.name}</li>}
                sx={{ width: 300 }}
                freeSolo
                renderInput={(params) => <TextField {...params} label={label} />}
            />
            <Dialog open={open} onClose={handleClose}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogTitle>Add a Field</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="id"
                            onChange={(event) =>
                                setValue("id", event.target.value)
                            }
                            label="Id"
                            type="text"
                            error={Boolean(errors['id']?.message)}
                            helperText={errors['id']?.message}
                        />
                        <hr/>
                        <TextField
                            margin="dense"
                            id="name"
                            defaultValue={getValues("name")}
                            onChange={(event) =>
                               setValue("name", event.target.value)
                            }
                            label="Name"
                            multiline
                            rows={4}
                            type="text"
                            error={Boolean(errors['name']?.message)}
                            helperText={errors['name']?.message}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>Cancel</Button>
                        <Button type="submit">Add</Button>
                    </DialogActions>
                </form>
            </Dialog>
        </React.Fragment>
    );
}

export default CustomSelect;