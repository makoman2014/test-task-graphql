import React, {useEffect, useState} from 'react';
import './App.css';
import CustomSelect, {OptionType} from "./components/CustomAutocomplite";
import {useQuery} from "@apollo/client";
import {GET_INDIVIDUAL_COMPANY_POSITIONS, GET_INDIVIDUAL_COMPANY_RELATIONS} from "./query/data";

function App() {
    const {data:companyRelations} = useQuery(GET_INDIVIDUAL_COMPANY_RELATIONS);
    const {data:companyPositions} = useQuery(GET_INDIVIDUAL_COMPANY_POSITIONS);
    const [optionsCompanyRelation , setOptionsCompanyRelation] = useState<OptionType[]>([]);
    const [optionsCompanyPositions , setOptionsCompanyPositions] = useState<OptionType[]>([]);

    useEffect(()=>{
        if(companyRelations?.applicantIndividualCompanyRelations?.data){
            setOptionsCompanyRelation(companyRelations.applicantIndividualCompanyRelations.data);
        }
    }, [companyRelations?.applicantIndividualCompanyRelations?.data])

    useEffect(()=>{
        if(companyPositions?.applicantIndividualCompanyPositions?.data){
            setOptionsCompanyPositions(companyPositions.applicantIndividualCompanyPositions.data);
        }
    }, [companyPositions?.applicantIndividualCompanyPositions?.data])


    const addNewRelationOption = (option: OptionType) => {
        setOptionsCompanyRelation([...optionsCompanyRelation, option])
    }
    const addNewPositionOption = (option: OptionType) => {
        setOptionsCompanyPositions([...optionsCompanyPositions, option])
    }

  return (
    <div className="app">
        <CustomSelect options={optionsCompanyRelation} addNewOption={addNewRelationOption} label="Select company relation"/>
        <CustomSelect options={optionsCompanyPositions} addNewOption={addNewPositionOption} label="Select company position"/>
    </div>
  );
}

export default App;
